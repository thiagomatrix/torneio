import React from 'react';
import ReactDOM from 'react-dom';

//import * as serviceWorker from './serviceWorker';

import {HashRouter} from 'react-router-dom'
import Routes from './main/Routes';
import Nav from './components/templates/Nav';
import Footer from './components/templates/Footer';
//import App from './main/App';

ReactDOM.render(
(
<HashRouter>
<Nav />
<Routes />
<Footer />
</HashRouter>), 

document.getElementById('root'));

//serviceWorker.unregister();
