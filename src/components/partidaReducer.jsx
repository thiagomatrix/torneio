const INIT_VALUE = {
    time_A:0,
    time_B:0,
    tempo: 0,
    init: new Date().getTime()
}

export default function (state=INIT_VALUE, action){
    switch (action.type) {
        case 'GOL_A':
            return {...state, time_A: state.time_A + 1}
        case 'GOL_B':
            return {...state, time_A: state.time_A + 1}
        case 'TEMPO':
            return {...state, tempo: new Date(state.init - new Date().getTime()).getMinutes()}
        default:
            break;
    }
}