const INIT_VALUE = {
    teams:[],
    players: []
}

export default function (state=INIT_VALUE, action){
    switch (action.type) {
        case 'CREATE_TEAM':
            return {...state, teams: state.teams + action.data }
        case 'UPDATE_TEAM':
            return {...state, teams: state.teams.action.id = action.data }
        case 'DELETE_TEAM':
            return {...state, teams: state.teams.action.id = null }
        case 'CREATE_PLAYER':
            return {...state, players: state.players + action.data }
        case 'UPDATE_PLAYER':
            return {...state, players: state.teams.action.id = action.data }
        case 'DELETE_PLAYER':
            return {...state, players: state.players.action.id = null }
        default:
            state
    }
}