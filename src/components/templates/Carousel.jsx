import React from 'react'
import './Carousel.css'
export default props=>
<div className="full-slider">
<div id="carousel-example-generic" className="carousel slide">

   <ol className="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
   </ol>

   <div className="carousel-inner" role="listbox">

      <div className="item active darkerskyblue" data-ride="carousel" data-interval="5000">
         <div className="carousel-caption">
            <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12"></div>
            <div className="col-lg-5 col-md-5 col-sm-12 col-xs-12">
               <div className="slider-contant" data-animation="animated fadeInRight">
                  <h3>Cadastre <span className="color-yellow">Seu time</span> e jogue</h3>
                  <p>Crie seus jogos e duele entre os times</p>
                  <button className="btn btn-primary btn-lg">Novo Jogo</button>
               </div>
            </div>
         </div>
      </div>

      <div className="item skyblue" data-ride="carousel" data-interval="5000">
         <div className="carousel-caption">
            <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12"></div>
            <div className="col-lg-5 col-md-5 col-sm-12 col-xs-12">
               <div className="slider-contant" data-animation="animated fadeInRight">
                  <h3>Crie<span className="color-yellow"> os seus </span>Jogadores</h3>
                  <p>Crie seus jogadores e duele entre os times</p>
                  <button className="btn btn-primary btn-lg">Criar Jogador</button>
               </div>
            </div>
         </div>
      </div>
     

   </div>

   <a className="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
   <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
   <span className="sr-only">Anterior</span>
   </a>
   <a className="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
   <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
   <span className="sr-only">Próximo</span>
   </a>
</div>


</div>

