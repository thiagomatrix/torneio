import React from 'react'
export default props=>
<div id="preloader">
<img className="preloader" src={process.env.PUBLIC_URL + 'assets/images/loading-img.gif'} alt="loader"/>
</div>