import './Nav.css'
import React from 'react'
import { NavLink } from 'react-router-dom'
//import Preloader from '../templates/Preloader'

export default props =>
//<Preloader />
<header>
<div className="container">
   <div className="header-top">
      <div className="row">
         <div className="col-md-6">
            <div className="full">
               <div className="logo">
                              <a href="/"><img src={process.env.PUBLIC_URL + 'assets/images/logo.png'} alt="logo" /></a>
                           </div>
                        </div>
                     </div>
                    {/* <div className="col-md-6">
                        <div className="right_top_section">
                           <ul className="login">
                              <li className="login-modal">
                                 <a href="/" className="login"><i className="fa fa-user"></i>Entrar</a>
                              </li>
                              <li>
                                 <div className="cart-option">
                                    <a href="/"><i className="fa fa-shopping-cart"></i>Cadastrar</a>
                                 </div>
                              </li>
                           </ul>
                        </div>
                    </div>*/}
                  </div>
               
<div className="header-bottom">
<div className="row">
<div className="col-md-12">
 <div className="full">
    <div className="main-menu-section">
       <div className="menu">
          <nav className="navbar navbar-inverse">
             <div className="navbar-header">
                <button className="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                </button>
                <a className="navbar-brand" href="/">Menu</a>
             </div>
             <div className="collapse navbar-collapse js-navbar-collapse">
                <ul className="nav navbar-nav">
                   <li><NavLink to="/" activeClassName="active" replace>Inicio</NavLink></li>
                   <li><NavLink to="/jogos" activeClassName="active" replace>Jogos</NavLink></li>
                   <li><NavLink to="/jogadores" activeClassName="active" replace>Jogadores</NavLink></li>
                   <li><NavLink to="/times" activeClassName="active" replace>Time</NavLink></li>
                   <li><NavLink to="/sobre" activeClassName="active" replace>Sobre</NavLink></li>
                </ul>
             </div>
          </nav>
       </div>
    </div>
 </div>
</div>
</div>

</div>
</div>
</div>
</header>




