import './Footer.css'
import React from 'react'
import { NavLink } from 'react-router-dom'

export default props =>
<footer id="footer" className="footer">
<div className="container">
   <div className="row">
      <div className="col-md-4">
         <div className="full">
            <div className="footer-widget">
               <div className="footer-logo">
                  <a href="/"><img alt="footer-logo" src={process.env.PUBLIC_URL + '/assets/images/footer-logo.png'} /></a>
               </div>
               <p>Sistema de torneio de Futebol.</p>
              
            </div>
         </div>
      </div>
      <div className="col-md-2">
         <div className="full">
            <div className="footer-widget">
               <h3>Menu</h3>
               <ul className="footer-menu">
                   <li><NavLink to="/" activeClassName="active" replace>Inicio</NavLink></li>
                   <li><NavLink to="/jogos" activeClassName="active" replace>Jogos</NavLink></li>
                   <li><NavLink to="/jogadores" activeClassName="active" replace>Jogadores</NavLink></li>
                   <li><NavLink to="/times" activeClassName="active" replace>Time</NavLink></li>
                   <li><NavLink to="/sobre" activeClassName="active" replace>Sobre</NavLink></li>
               </ul>
            </div>
         </div>
      </div>
      <div className="col-md-3">
         <div className="full">
            <div className="footer-widget">
               <h3>Sobre</h3>
               <ul className="address-list">
                  <li><i className="fa fa-map-marker"></i> Thiago Braga</li>
                  <li><i className="fa fa-phone"></i> (21) 96765-0961</li>
                  <li><i style={{fontsize: 20,top:5}}className="fa fa-envelope"></i> thiagomatrix@gmail.com</li>
               </ul>
            </div>
         </div>
      </div>
      <div className="col-md-3">
         <div className="full">
       
         </div>
      </div>
   </div>
</div>
<div className="footer-bottom">
   <div className="container">
      <p>Copyright © 2019 Todos os Direiros Reservados a <a href="/" target="_blank">Thiago Braga</a></p>
   </div>
</div>
</footer>