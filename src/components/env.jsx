import React from 'react'

const debug = true
const URL_LOCAL = '127.0.0.1:8080/torneio'
const URL = 'https://torneiro-back.develup.com.br'
export default class ENV extends React.Component{
    if(debug){
        URL = URL_LOCAL
    }
} 