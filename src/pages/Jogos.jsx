import React from 'react';
import './utils/App.css';
import '../components/templates/Jogos.css';
import ModalJogos from '../components/templates/ModalJogos';

function Game() {
  return (
    document.title = 'Torneio - Jogos',
    <ModalJogos />,
    <div className="App top">
    <div className="inner-jogos-banner">
            <div className="container">
            </div>
         </div>
    
    <div className="inner-information-text">
            <div className="container">
               <h3>Jogos</h3>
               <ul className="breadcrumb">
                  <li><a href="/">Início</a></li>
                  <li className="active">Jogos</li>
               </ul>
            </div>
         </div>

        <div className="col-md-11">
            <div className="right_top_section">
            <button className="btn btn-primary btn-lg">Novo Jogo</button>
            </div>
        </div>

         <section id="contant" className="contant main-heading team">
         <div className="row">
            <div className="container">

               <div className="col-md-6 column">
                  <div className="card">
                  <div className="team-btw-match duelo">
                           <ul>
                              <li>
                                 <img className="escudo" alt='flamengo' src={process.env.PUBLIC_URL + '/assets/images/times/flamengo.png' }/>
                                 <span>Flamengo</span>
                                 <div className="result">0</div>
                              </li>
                              <li className="vs"><span>vs</span></li>
                              <li>
                                 <img className="escudo" alt='vasco' src={process.env.PUBLIC_URL + '/assets/images/times/vasco.png' }/>
                                 <span>Vasco</span>
                                 <div className="result">0</div>
                              </li>
                           </ul>
                        </div>
                     <div className="title-card">
                     
                        <p className="title">Data: 09/11/2019 as 15:00</p>
                        <p>
                        </p><div className="center"><button className="button">Iniciar Partida</button></div>
                        <p></p>
                     </div>
                  </div>
               </div>

                   <div className="col-md-6 column">
                  <div className="card">
                  <div className="team-btw-match duelo">
                           <ul>
                              <li>
                                 <img className="escudo" alt='gremio' src={process.env.PUBLIC_URL + '/assets/images/times/gremio.png' }/>
                                 <span>Grêmio</span>
                                 <div className="result">0</div>
                              </li>
                              <li className="vs"><span>vs</span></li>
                              <li>
                                 <img className="escudo" alt='chapecoense' src={process.env.PUBLIC_URL + '/assets/images/times/chapecoense.png' }/>
                                 <span>Chapecoense</span>
                                 <div className="result">0</div>
                              </li>
                           </ul>
                        </div>  <div className="title-card">
                     
                        <p className="title">Data: 09/11/2019 as 15:00</p>
                        <p>
                        </p><div className="center"><button className="button">Iniciar Partida</button></div>
                        <p></p>
                     </div>
                  </div>
               </div>

   <div className="col-md-6 column">
                  <div className="card">
                  <div className="team-btw-match duelo">
                           <ul>
                              <li>
                                 <img className="escudo" alt='fluminense' src={process.env.PUBLIC_URL + '/assets/images/times/fluminense.png' }/>
                                 <span>Fluminense</span>
                                 <div className="result">0</div>
                              </li>
                              <li className="vs"><span>vs</span></li>
                              <li>
                                 <img className="escudo" alt='cruzeiro' src={process.env.PUBLIC_URL + '/assets/images/times/cruzeiro.png' }/>
                                 <span>Cruzeiro</span>
                                 <div className="result">0</div>
                              </li>
                           </ul>
                        </div>   <div className="title-card">
                     
                        <p className="title">Data: 09/11/2019 as 15:00</p>
                        <p>
                        </p><div className="center"><button className="button">Iniciar Partida</button></div>
                        <p></p>
                     </div>
                  </div>
               </div>

   <div className="col-md-6 column">
                  <div className="card">
                  <div className="team-btw-match duelo">
                           <ul>
                              <li>
                                 <img className="escudo" alt='santos' src={process.env.PUBLIC_URL + '/assets/images/times/santos.png' }/>
                                 <span>Santos</span>
                                 <div className="result">0</div>
                              </li>
                              <li className="vs"><span>vs</span></li>
                              <li>
                                 <img className="escudo" alt='sao-paulo' src={process.env.PUBLIC_URL + '/assets/images/times/sao-paulo.png' }/>
                                 <span>São Paulo</span>
                                 <div className="result">0</div>
                              </li>
                           </ul>
                        </div>   <div className="title-card">
                     
                        <p className="title">Data: 08/11/2019 as 15:00</p>
                        <p>
                        </p><div className="center"><button className="button bt-danger">finalizada</button></div>
                        <p></p>
                     </div>
                  </div>
               </div>


                </div>
         </div>
      </section>
    
    </div>
  );
}

export default Game;