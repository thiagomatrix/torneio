import React from 'react';
import './utils/App.css';

function Sobre() {
  return (
    document.title = 'Torneio - Sobre',
    <div className="App top">
    <div class="inner-sobre-banner">
            <div class="container">
            </div>
         </div>
    
    <div className="inner-information-text">
            <div className="container">
               <h3>Sobre</h3>
               <ul className="breadcrumb">
                  <li><a href="/">Início</a></li>
                  <li className="active">Sobre</li>
               </ul>
            </div>
         </div>

         <section id="contant" class="contant main-heading" style={{paddingbottom:0, marginbottom:-1, position: 'relative', zindex:1}}>
         <div class="aboutus">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 col-sm-12">
                     <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="full">
                           <h3>Sistema de Torneio de Futebol</h3>
                           <p>Torneio o Jogo de Futebol foi desenvolvido para controlar um torneio onde dse cadastra os times e seus respectivos jogadores e se iniciar uma partida com dois times cadastrados e com 5 jogadores em cada time.
                           Ao iniciar um jogo, você escolhe dois times que irão duelar e informar o placar final da partida, e conheça assim o time vencedor.
                           </p>
                           <ul class="icon-list">
                              <li><i class="fa fa-angle-right"></i> O Front-end foi desenvolvido em React/Redux.</li>
                              <li><i class="fa fa-angle-right"></i> O Back-end foi desenvolvida uma API, com serviços REST, utilizando .Net Core</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-5 col-sm-5 col-xs-12">
                        <img class="img-responsive" src="assets/images/img-02_002.jpg" alt="#"/>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        
          
</section>
</div>
 
  );
}

export default Sobre;