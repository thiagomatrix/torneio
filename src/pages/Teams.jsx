import React from 'react';
import './utils/App.css';
import ModalJogadores from '../components/templates/ModalJogadores';

function Teams() {
  return (
    document.title = 'Torneio - Jogadores',
    <ModalJogadores />,
    <div className="App top">
    <div class="inner-jogadores-banner">
            <div class="container">
            </div>
    </div>
    
    <div className="inner-information-text">
            <div className="container">
               <h3>Jogadores</h3>
               <ul className="breadcrumb">
                  <li><a href="/">Início</a></li>
                  <li className="active">Jogadores</li>
               </ul>
            </div>
         </div>

            <div className="col-md-11">
            <div className="right_top_section">
            <button className="btn btn-primary btn-lg">Novo Jogador</button>
            </div>
            </div>

         <section id="contant" className="contant main-heading team">
         <div className="row">
            <div className="container">
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-4.jpg" alt="John" />
                     <div className="title-card">
                        <h4>John Doe</h4>
                        <p className="title">Volante</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-2.jpg" alt="Mike"/>
                     <div className="title-card">
                        <h4>Mike Ross</h4>
                        <p className="title">Zagueiro</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-3.jpg" alt="John"/>
                     <div className="title-card">
                        <h4>John Doe</h4>
                        <p className="title">Atacante</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-4.jpg" alt="John"/>
                     <div className="title-card">
                        <h4>John Doe</h4>
                        <p className="title">Lateral Esquerdo</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-2.jpg" alt="Mike"/>
                     <div className="title-card">
                        <h4>Mike Ross</h4>
                        <p className="title">Lateral Direiro</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-3.jpg" alt="John"/>
                     <div className="title-card">
                        <h4>John Doe</h4>
                        <p className="title">Goleiro</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-3.jpg" alt="John"/>
                     <div className="title-card">
                        <h4>John Doe</h4>
                        <p className="title">Centro Avante</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-4.jpg" alt="John"/>
                     <div className="title-card">
                        <h4>John Doe</h4>
                        <p className="title">Alas Direito</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive w-100" src="assets/images/img-1-4.jpg" alt="John"/>
                     <div className="title-card">
                        <h4>John Doe</h4>
                        <p className="title">Alas Esquerdo</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        <button className="button bt-danger">Apagar</button>
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
    
    </div>
 
  );
}

export default Teams;