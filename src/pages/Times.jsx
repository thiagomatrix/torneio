import React from 'react';
import './utils/App.css';
import ModalTimes from '../components/templates/ModalTimes';

function Times() {
  return (
    document.title = 'Torneio - Times',
    <ModalTimes />,
    <div className="App top">
    <div className="inner-time-banner">
            <div className="container">
            </div>
         </div>
    
    <div className="inner-information-text">
            <div className="container">
               <h3>Times</h3>
               <ul className="breadcrumb">
                  <li><a href="/">Início</a></li>
                  <li className="active">Times</li>
               </ul>
            </div>
         </div>

         <section id="contant" className="contant main-heading team">
         <div className="row">
            <div className="container">
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/americamg.png" alt="John" />
                     <div className="title-card">
                        <h4>América MG</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p>
                        <div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274 " src="assets/images/times/america-rj.png" alt="Mike"/>
                     <div className="title-card">
                        <h4>América RJ</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/atletico-goianiense.png" alt="John"/>
                     <div className="title-card">
                        <h4>Atlético Goianiense</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/atletico-mg.png" alt="John"/>
                     <div className="title-card">
                        <h4>Atlético Mineiro</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/atletico-pr.png" alt="Mike"/>
                     <div className="title-card">
                        <h4>Atlético Paranaense</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274 " src="assets/images/times/avai.png" alt="John"/>
                     <div className="title-card">
                        <h4>Avai</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274"   src="assets/images/times/bahia.png" alt="bahia"/>
                     <div className="title-card">
                      <h4>Bahia</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/botafogo.png" alt="bangu"/>
                     <div className="title-card">
                        <h4>Botafogo</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/brasil-pelotas.png" alt="bangu"/>
                     <div className="title-card">
                        <h4>Pelotas</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/ceara.png" alt="bangu"/>
                     <div className="title-card">
                        <h4>Ceará</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/chapecoense.png" alt="bangu"/>
                     <div className="title-card">
                        <h4>Chapecoense</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/corinthians.png" alt="corinthians"/>
                     <div className="title-card">
                        <h4>Corinthians</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/coritiba.png" alt="coritiba"/>
                     <div className="title-card">
                        <h4>Coritiba</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/crb.png" alt="crb"/>
                     <div className="title-card">
                        <h4>CRB</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/criciuma.png" alt="criciuma"/>
                     <div className="title-card">
                        <h4>Criciúma</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/cruzeiro.png" alt="cruzeiro"/>
                     <div className="title-card">
                        <h4>Cruzeiro</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/csa.png" alt="csa"/>
                     <div className="title-card">
                        <h4>CSA</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/ferroviaria.png" alt="ferroviaria"/>
                     <div className="title-card">
                        <h4>Ferroviária</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/figueirense.png" alt="figueirense"/>
                     <div className="title-card">
                        <h4>Figueirense</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/flamengo.png" alt="flamengo"/>
                     <div className="title-card">
                        <h4>Flamengo</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/fluminense.png" alt="fluminense"/>
                     <div className="title-card">
                        <h4>Fluminense</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/fortaleza.png" alt="fortaleza"/>
                     <div className="title-card">
                        <h4>Fortaleza</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/gama.png" alt="gama"/>
                     <div className="title-card">
                        <h4>Gama</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/goias.png" alt="goias"/>
                     <div className="title-card">
                        <h4>Goias</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/bangu.png" alt="gremio"/>
                     <div className="title-card">
                        <h4>Gremio</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/guarani.png" alt="guarani"/>
                     <div className="title-card">
                        <h4>Guarani</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/internacional.png" alt="internacional"/>
                     <div className="title-card">
                        <h4>Intenacional</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/joinville.png" alt="joinville"/>
                     <div className="title-card">
                        <h4>Joinville</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/nautico.png" alt="nautico"/>
                     <div className="title-card">
                        <h4>Nautico</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/palmeiras.png" alt="palmeiras"/>
                     <div className="title-card">
                        <h4>Palmeiras</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/paysandu.png" alt="paysandu"/>
                     <div className="title-card">
                        <h4>Paysandu</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/portuguesa.png" alt="portuguesa"/>
                     <div className="title-card">
                        <h4>Portuguesa</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/santa-cruz.png" alt="santa-cruz"/>
                     <div className="title-card">
                        <h4>Santa Cruz</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/santa-andre.png" alt="santa-andre"/>
                     <div className="title-card">
                        <h4>Santo André</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/santos.png" alt="santos"/>
                     <div className="title-card">
                        <h4>Santos</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/sao-caeano.png" alt="sao-caetano"/>
                     <div className="title-card">
                        <h4>São Caetano</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/sao-paulo.png" alt="sao-paulo"/>
                     <div className="title-card">
                        <h4>São Paulo</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/sport.png" alt="sport"/>
                     <div className="title-card">
                        <h4>Sport</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/vasco.png" alt="vasco"/>
                     <div className="title-card">
                        <h4>Vasco</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/vila-nova.png" alt="vila-nova"/>
                     <div className="title-card">
                        <h4>Vila Nova</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>
               <div className="col-md-3 column">
                  <div className="card">
                     <img className="img-responsive times mh-274" src="assets/images/times/vitoria.png" alt="vitoria"/>
                     <div className="title-card">
                        <h4>Vitória</h4>
                        <p className="title">0 Jogadores</p>
                        <p>
                        </p><div className="center">
                        <button className="button mr-10">Editar</button>
                        
                        </div>
                        <p></p>
                     </div>
                  </div>
               </div>

               
            </div>
         </div>
      </section>
    
    </div>
 
  );
}

export default Times;