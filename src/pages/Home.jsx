import React from 'react';
import Carousel from '../components/templates/Carousel'
import '../components/templates/Home.css'

const  Home = props => {
  return (
    document.title = 'Torneio - Bem-vindo',
    <div>
      <Carousel />

      <div class="matchs-info">
         <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
               <div class="full">
                  <div class="matchs-vs">
                     <div class="vs-team">
                        <div class="team-btw-match">
                           <ul>
                              <li>
                                 <img width="100px" alt='chapecoense' src={process.env.PUBLIC_URL + '/assets/images/times/chapecoense.png' }/>
                                 <span>Chapecoense</span>
                              </li>
                              <li class="vs"><span>vs</span></li>
                              <li>
                                 <img width="100px"  alt='vasco' src={process.env.PUBLIC_URL + '/assets/images/times/vasco.png' }/>
                                 <span>Vasco</span>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
               <div class="full">
                  <div class="right-match-time">
                     <h2>Próximo Jogo </h2>
                     <ul id="countdown-1" class="countdown">
                        <li><span class="days">10 </span>dias </li>
                        <li><span class="hours">5 </span>Horas </li>
                        <li><span class="minutes">25 </span>Minutos </li>
                     </ul>
                     <span>12/02/2017 as 19:00pm</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <section id="contant" className="contant">
      <div className="container">
      
        <div className="row">
            <div className="col-lg-4 col-sm-4 col-xs-12">
            <aside id="sidebar" className="left-bar">
                     <div className="banner-sidebar">
                        <img className="img-responsive" src={process.env.PUBLIC_URL + 'assets/images/img-05.jpg'} alt="imagem5" />
                        <h3>Monte Seu Time</h3>
                     </div>
                  </aside>
                  <h4>Próximos eventos</h4>
                  <aside id="sidebar" className="left-bar">
                     <div className="feature-matchs">
                        <div className="team-btw-match">
                        <ul>
                          <li>
                            <img width="50px" src={process.env.PUBLIC_URL + 'assets/images/times/flamengo.png'} alt="flamengo"/><span>Flamengo</span>
                            </li>
                          <li className="vsu"><span>vs</span></li>
                              <li>
                                 <img width="50px"  src={process.env.PUBLIC_URL + 'assets/images/times/vasco.png'} alt="vasco"/><span>Vasco</span>
                              </li>
                        </ul>
                        </div>
                      </div>
                  </aside>
           
            
            </div>
            <div className="col-lg-8 col-sm-8 col-xs-12">
      <h4>Histórico de Jogos</h4>
      <aside id="sidebar" class="left-bar">
                     <div class="feature-matchs">
                        <table class="table table-bordered table-hover alig-center">
                           <thead>
                              <tr>
                                 <th className="alig-center">Time</th>
                                 <th className="alig-center">Placar</th>
                                 <th className="alig-center">Data</th>
                                 <th className="alig-center">Estado</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                               
                                 <td>                  
                              <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/flamengo.png'} alt="flamengo"/><span>Flamengo</span>
                              <span className="vsu"> vs </span>
                              <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/vasco.png'} alt="vasco"/><span>Vasco</span>
                                </td>
                                 <td>1 x 0</td>
                                 <td>09/11/2019 as 14:00h</td>
                                 <td>Iniciado</td>
                              </tr>
                              <tr>
                               
                                 <td> 
                                 <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/flamengo.png'} alt="flamengo"/><span>Flamengo</span>
                              <span className="vsu"> vs </span>
                              <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/vasco.png'} alt="vasco"/><span>Vasco</span>
                                  </td>
                                 <td>1 x 1</td>
                                 <td>09/11/2019 as 15:00h</td>
                                 <td>Terminado</td>
                              </tr>
                              <tr>
                                
                                 <td> 
                                 <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/flamengo.png'} alt="flamengo"/><span>Flamengo</span>
                              <span className="vsu"> vs </span>
                              <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/vasco.png'} alt="vasco"/><span>Vasco</span>
                                  </td>
                                 <td>0 x 0</td>
                                 <td>10/11/2019 as 13:00h</td>
                                 <td>Agendado</td>
                              </tr>
                              <tr>
                              
                                 <td>
                                 <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/flamengo.png'} alt="flamengo"/><span>Flamengo</span>
                              <span className="vsu"> vs </span>
                              <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/vasco.png'} alt="vasco"/><span>Vasco</span>
                                  </td>
                                 <td>0 x 0</td>
                                 <td>10/11/2019 as 13:00h</td>
                                 <td>Agendado</td>
                              </tr>
                              <tr>
                                
                                 <td> 
                                 <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/flamengo.png'} alt="flamengo"/><span>Flamengo</span>
                              <span className="vsu"> vs </span>
                              <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/vasco.png'} alt="vasco"/><span>Vasco</span>
                                  </td>
                                 <td>0 x 1</td>
                                 <td>09/11/2019 as 13:00h</td>
                                 <td>Terminado</td>
                              </tr>
                             <tr>
                                 <td> 
                                 <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/flamengo.png'} alt="flamengo"/><span>Flamengo</span>
                              <span className="vsu"> vs </span>
                              <img height="19px" src={process.env.PUBLIC_URL + 'assets/images/times/vasco.png'} alt="vasco"/><span>Vasco</span>
                                 </td>
                                 <td>0 x 0</td>
                                 <td>09/11/2019 as 13:00h</td>
                                 <td>Iniciado</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </aside>
      </div>
    

        </div>
      </div>
      
      </section>
    
    </div>
       );
}

export default Home;