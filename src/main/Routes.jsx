import React from 'react'
import {Switch, Route, Redirect} from 'react-router'

import Home from '../pages/Home';
import Teams from '../pages/Teams';
import Times from '../pages/Times';
import Jogos from '../pages/Jogos';
import Sobre from '../pages/Sobre';


export default props =>
<Switch>
<Route exact path='/' component={Home} />
<Route path='/jogos' component={Jogos}/>
<Route path='/jogadores' component={Teams}/>
<Route path='/times' component={Times}/>
<Route path='/sobre' component={Sobre}/>

<Redirect from="*" to="/"/>       
</Switch>