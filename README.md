@thiagobraga:

> Thiago Braga
> thiagomatrix@gmail.com

**Api REST .Net Core e React**

Api em .Net (Backend) e Sistema de **Torneio de Futebol**, desenvolvido em react de forma interativa para fazer o CRUD de Jogadores, Partidas, Times e Duelo entre as equipes.

---

**Requerimentos**

Para a utilização desse projeto são necessários a instalacão de:

1. nodeJS
2. .net core

---

**Recomendações**

Para a ultização desse projeto são recomentados os seguintes programas:

1. Virtual Studio (para visualizacão de código e comandos)
2. Postman (para teste de Rest API)

---

**Direitos Autorais**

Esse é um projeto Alpha e OpenSource, construido apenas para demostração.

*Por se tratar de um projeto Alpha, não é aconselhável o seu uso para ambiente de produção. O uso desse sistema de torneios  / api é estritamente de responsabilidade de seu usuário.*

---

## Instalação do NodeJS

Instale o interpretador Python no seu computador.

1. Faça download do **Node JS 12.x** LTS no sistema operacional desejado. <br>Baixe agora o NODE JS [Download NodeJS](https://nodejs.org/en/).
2. Feche e Abra o **Terminal**.
3. Execute o comando  `node --version.`
4. Verifique se a versão mostrada é maior do que a 11.xx.

---

## Clonar o repositorio

Para clonar o repositório, execute o comando `git clone URL` no terminar em um diretório que deseja copiar.  [URL](https://bitbucket.org/thiagomatrix/torneio)

---

### Instalação

1. Após a instalação do node JS, Instale as dependências do projeto, execute o seguinte comando na pasta raiz do projeto: `npm install`


### Execução

1. execute o comando `npm start` na pasta raiz do projeto
2. acesse o [link](http://localhost:3000) localhost:3000 

            }
##### Sistema de Torneio (Frontend):
http://127.0.0.1:3000/
  

